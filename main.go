package main

import (
	"bitbucket.org/legitimate/school-management-backend/app"
	"bitbucket.org/legitimate/school-management-backend/config"
)

func main() {

	config := config.GetConfig()

	app := &app.App{}
	app.Initialize(config)
	app.Run(":3000")
}
